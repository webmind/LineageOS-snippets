#!/system/xbin/bash

echo `date`" Started debian boot script" >> /data/cache/debian.log

while [ `getprop sys.boot_completed` -eq 0 ]; do
    sleep 2;
    echo `date`" Waiting for sys.boot_completed..."
done

while [ ! -e /sdcard/debian.img ]; do
    sleep 2;
    echo `date`" Waiting for /sdcard/debian.img..." >> /data/cache/debian.log
done



if [ ! -d /mnt/debian ]; then 
    mkdir -p /mnt/debian
fi
if [ -e /sdcard/debian.img ]; then
    export DEBLOOP=`/system/bin/losetup -sf /sdcard/debian.img 2>> /data/cache/debian.log`
    echo `date`" /sdcard/debian.img => ${DEBLOOP}" >> /data/cache/debian.log
else
    echo `date`" ERROR: /sdcard/debian.img does not exist!" >> /data/cache/debian.log
    exit;
fi
if [ -e "${DEBLOOP}" ]; then
    echo `date`" Mounting ${DEBLOOP} on /mnt/debian" >> /data/cache/debian.log
    /system/bin/mount ${DEBLOOP} /mnt/debian >> /data/cache/debian.log 2>&1 || echo "Failed Mount" >> /data/cache/debian.log
else
    echo `date`" ERROR: [${DEBLOOP}] does not exist." >> /data/cache/debian.log
    exit;
fi
if [ -e /mnt/debian/etc/rc.conf ]; then
    echo `date`" openrc sysinit..." >> /data/cache/debian.log
    chroot /mnt/debian /sbin/openrc sysinit >> /data/cache/debian.log 2>&1
    echo `date`" openrc default..." >> /data/cache/debian.log
    chroot /mnt/debian /sbin/openrc default >> /data/cache/debian.log 2>&1
else
    echo `date`" ERROR: /mnt/debian/etc/rc.conf does not exist!" >> /data/cache/debian.log
    exit
fi
echo `date`" Finished debian start" >> /data/cache/debian.log
